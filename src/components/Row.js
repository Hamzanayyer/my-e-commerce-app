import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { removeFromCart, updateCart } from "../app/lib/actions";

const Row = (props) => {
  const { quantity, details, id } = props.item;
  const item = details;
  const [qty, setQty] = useState(quantity);
  const dispatch = useDispatch();
  const update = (action) => {
    if (action === "increment") {
      setQty(qty + 1);
    }
    if (action === "decrement") {
      setQty(qty - 1);
    }
  };

  const remove = (id) => {
    dispatch(removeFromCart(id));
  };

  useEffect(() => {
    dispatch(updateCart(id, qty));
  }, [qty]);

  return (
    <tr>
      <td>
        <img
          width='70'
          height='70'
          src={
            process.env.PUBLIC_URL + `/assets/${item.category}/${item.image}`
          }
          alt={item.name}
        />
      </td>
      <td>{item.ref}</td>
      <td>€{item.price}</td>
      <td>
        <div className='btn-group' role='group' aria-label='Basic example'>
          <button
            type='button'
            className='btn btn-secondary'
            onClick={() => {
              if (qty > 1) {
                update("decrement");
              }
            }}>
            -
          </button>
          <span className='btn btn-light'>{qty}</span>
          <button
            type='button'
            className='btn btn-secondary'
            onClick={() => {
              update("increment");
            }}>
            +
          </button>
        </div>
      </td>
      <td>€{(quantity * item.price).toFixed(2)}</td>
      <td>
        <button
          onClick={() => remove(id)}
          type='button'
          className='btn btn-danger remove'>
          X
        </button>
      </td>
    </tr>
  );
};

export default Row;
