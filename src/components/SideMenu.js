import React from "react";
const SideMenu = ({ loadCategory, category }) => {
  const links = [
    "Fruits",
    "Légumes",
    "Produits frais",
    "Epiceriie",
    "Boissons",
  ];

  return (
    <div className='col-sm-2 sidebar'>
      <ul>
        {links.map((link, index) => {
          return (
            <li
              key={index}
              className={category === index ? "active" : ""}
              onClick={() => loadCategory(index)}>
              {link}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default SideMenu;
