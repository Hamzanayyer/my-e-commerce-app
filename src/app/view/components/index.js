import React from "react";
import "../../styles/App.css";
import { Navbar } from "../../../components/Navbar";
import { listOfProducts } from "../../../data";
import { BrowserRouter as Router, Route } from "react-router-dom";
import CartPage from "./CartPage";
import Home from "./Home";
const App = () => {
  const [category, setCategory] = React.useState(0);
  const [isFiltering, setFiltering] = React.useState(false);
  const [filtered, setFiltered] = React.useState(false);
  const loadCategory = (index) => setCategory(index);
  const filterResults = (event) => {
    let fullList = listOfProducts.flat();
    let results = fullList.filter((item) => {
      const name = item.name.toLowerCase();
      const term = event.toLowerCase();
      return name.indexOf(term) > -1;
    });
    setFiltered(results);
  };

  React.useEffect(() => {
    console.log(isFiltering);
  });

  return (
    <>
      <Router>
        <Navbar filter={filterResults} setFiltering={setFiltering} />
        <Route
          exact
          path='/'
          component={() => (
            <Home
              category={category}
              loadCategory={loadCategory}
              list={listOfProducts}
              isFiltering={isFiltering}
              filtered={filtered}
            />
          )}
        />
        <Route path='/cart' component={CartPage} />
      </Router>
    </>
  );
};
export default App;
