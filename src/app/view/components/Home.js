import React from "react";
import SideMenu from "../../../components/SideMenu";
import { List } from "../../../components/List";

const Home = ({ category, loadCategory, isFiltering, filtered, list }) => {
  return (
    <div>
      <div className='container'>
        <div className='row'>
          <SideMenu loadCategory={loadCategory} category={category} />
          <div className='col-sm'>
            <div className='row'>
              <List
                category={category}
                data={isFiltering ? filtered : list[category]}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
